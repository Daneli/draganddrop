import { Box, Paper, List } from '@mui/material'

// import { EntryStatus } from '../../interfaces/entry'
// import { EntriesContext } from '../../context/entries/EntriesContext';
import { EntryCard } from './EntryCard'

// interface Props {
//     status: EntryStatus;
// }

export const EntryList = () => {

    // const { entries } = useContext(EntriesContext)
    // const entriesByStatus = useMemo(() => entries.filter(entry => (entry.status === status)), [entries, status])

    return (
        <Box>
            <Paper sx={{ height: 'calc(100vh - 250px', overflow: 'scroll', backgroundColor: 'transparent', padding: '1px 5px' }}>
                <List sx={{ opacity: 1 }}>
                     {/* {
                        entriesByStatus.map(entry => ( */}
                            <EntryCard />
                        {/* ))
                    } */}

                </List>
            </Paper>
        </Box>
    )
}