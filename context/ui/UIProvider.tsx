import { FC, useReducer } from 'react';

import { UIContext, UIReducer } from '.';

export interface UIState {
sidemenuOpen: boolean;
}

const UI_INITIAL_STATE: UIState = {
sidemenuOpen: false,
}

export const UIProvider: FC = () => {

const [state, dispatch] = useReducer(UIReducer, UI_INITIAL_STATE)

// const openSideMenu = () => {
//     dispatch({type: 'UI-OpenSidebar'})
// }

// const closeSideMenu = () => {
//     dispatch({type: 'UI-CloseSidebar'})
// }

   return (
       <UIContext.Provider value={{
        sidemenuOpen: false
       }}>
       </UIContext.Provider>
    )
}