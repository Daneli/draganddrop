import { FC, useReducer } from 'react';
import { v4 as uuidv4 } from 'uuid';

import { EntriesContext, entriesReducer } from './';

import { Entry } from '../../interfaces';

export interface EntriesState {
    entries: Entry[];
}

const Entries_INITIAL_STATE: EntriesState = {
    entries: [
        {
            _id: uuidv4(),
            description: 'Pendiente: Esto es un texto de prueba',
            status: 'pending',
            createdAt: Date.now()
        },
        {
            _id: uuidv4(),
            description: 'Progreso: Esto es un texto de prueba numero dos',
            status: 'in-progress',
            createdAt: Date.now() - 1000000
        },
        {
            _id: uuidv4(),
            description: 'Finzalizado: Esto es un texto de prueba numero tres',
            status: 'finished',
            createdAt: Date.now() - 100000
        },
    ]
}

export const EntriesProvider: FC = ({ }) => {

const [state, dispatch] = useReducer(entriesReducer, Entries_INITIAL_STATE)

   return (
       <EntriesContext.Provider value={{
           ...state
       }}>

       </EntriesContext.Provider>
    )
}