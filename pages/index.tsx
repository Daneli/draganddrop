import { NextPage } from "next";
import { Card, CardHeader, Grid, } from "@mui/material";

import { Layout } from '../components/layout/Layout'
import { EntryList } from "../components/ui/EntryList";

const HomePage: NextPage = () => {
  return (
    <>
      <Layout title="open jira">
        <Grid container spacing={2}>
          <Grid item xs={12} sm={4} >
            <Card sx={{ height: 'calc(100vh - 100px)' }}>
              <CardHeader title='Pendientes' />
              <EntryList />
            </Card>
          </Grid>
          <Grid item xs={12} sm={4} >
            <Card sx={{ height: 'calc(100vh - 100px)' }}>
              <CardHeader title='En progreso' />
              <EntryList />
            </Card>
          </Grid>
          <Grid item xs={12} sm={4} >
            <Card sx={{ height: 'calc(100vh - 100px)' }}>
              <CardHeader title='Completados' />
              <EntryList />
            </Card>
          </Grid>
        </Grid>
      </Layout>
    </>
  )
}

export default HomePage